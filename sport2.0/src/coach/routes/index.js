import Vue from 'vue'
import CoachHomepage from 'coach/views/homepage'


import CoachWrite from 'coach/views/write'
import CoachWritePlan from 'coach/views/write-plan'
import CoachWriteInfo from 'coach/views/write-info'

import CoachHistory from 'coach/views/history'
import CoachHistoryPlan from 'coach/views/history-plan'
import CoachHistoryReply from 'coach/views/history-reply'
import CoachHistoryInfo from 'coach/views/history-info'

import CoachPlanDetail from 'coach/views/plan-detail'
import CoachReplyDetail from 'coach/views/reply-detail'
import CoachInfoDetail from 'coach/views/info-detail'
import CoachDiaryReply from 'coach/views/diary-reply'
import CoachProfileEdit from 'coach/views/profile-edit'

import CoachMessage from 'coach/views/message'
import CoachMessageDetail from 'coach/views/message-detail'
import CoachUser from 'coach/views/user'

import About from 'common/views/about'
import Password from 'common/views/password'
import Help from 'common/views/help'
import Feedback from 'common/views/feedback'
import Login from 'common/views/login'
import Logout from 'common/views/logout'
import Success from 'common/views/success'

const routes = [
  {
    path: '*', 
    redirect: '/profile/edit'
  },
  {
    path: '/profile/edit',
    name: 'profile-edit',
    component: CoachProfileEdit
  },
  {
    path: '/homepage',
    name: 'homepage',
    component: CoachHomepage
  },
  {
    path: '/user',
    name: 'user',
    component: CoachUser
  },
  {
    path: '/history',
    name: 'history',
    component: CoachHistory,
    children: [
      {
        path: 'reply',
        name: 'history-reply',
        component: CoachHistoryReply
      },
      {
        path: 'plan',
        name: 'history-plan',
        component: CoachHistoryPlan
      },
      {
        path: 'info',
        name: 'history-info',
        component: CoachHistoryInfo
      }
    ]
  },
  {
    path: '/write',
    name: 'write',
    component: CoachWrite,
    children: [
      {
        path: 'plan',
        name: 'write-plan',
        component: CoachWritePlan
      },
      {
        path: 'info',
        name: 'write-info',
        component: CoachWriteInfo
      }
    ]
  },
  {
    path: '/info/detail',
    name: 'info-detail',
    component: CoachInfoDetail
  },
  {
    path: '/reply/detail',
    name: 'reply-detail',
    component: CoachReplyDetail
  },
  {
    path: '/diary/reply',
    name: 'diary-reply',
    component: CoachDiaryReply
  },
  {
    path: '/plan/detail',
    name: 'plan-detail',
    component: CoachPlanDetail
  },
  {
    path: '/message',
    name: 'message',
    component: CoachMessage
  },
  {
    path: '/message/detail',
    name: 'message-detail',
    component: CoachMessageDetail
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/password',
    name: 'passwrd',
    component: Password
  },
  {
    path: '/help',
    name: 'help',
    component: Help
  },
  {
    path: '/feedback',
    name: 'feedback',
    component: Feedback
  },
  {
    path: '/success',
    name: 'success',
    component: Success
  }
]

export default routes