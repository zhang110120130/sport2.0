/**
 * 日记相关
 * 1. 日记回复历史
 * 2. 回复日记
 */

import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { API_DIARY_REPLY_HISTORY, API_DIARY_REPLY_SUBMIT, API_DIARY_REPLY_DETAIL } from 'coach/constants/api'
import { LOAD_DIARY_REPLY_HISTORY, LOAD_DIARY_REPLY_HISTORY_FAIL,
         LOAD_DIARY_REPLY_HISTORY_SUCCESS, SUBMIT_DIARY_REPLY,
         SUBMIT_DIARY_REPLY_FAIL, SUBMIT_DIARY_REPLY_SUCCESS,
         LOAD_DIARY_REPLY, LOAD_DIARY_REPLY_SUCCESS,
         LOAD_DIARY_REPLY_FAIL, FLUSH_DIARY_REPLY } from 'coach/constants/action-types'
import createGetters from 'common/utils/create-getters'
import { StorageMaster } from 'common/utils/storage'

const state = {
  loadDiaryReplyHistoryStatus: DEFAULT,
  loadDiaryReplyHistoryMessage: '',
  loadDiaryReplyHistoryTotal: 1,
  loadDiaryReplyHistoryQuery: {
    pageSize: 10,
    pageNo: 1
  },
  diaryReplyList: [],
  loadDiaryReplyStatus: DEFAULT,
  loadDiaryReplyMessage: '',
  submitDiaryReplyStatus: DEFAULT,
  submitDiaryReplyMessage: '',
  diaryReply: null
}

const getters = createGetters(state)

const actions = {
  flushDiaryReply ({commit, state}) {
    commit(FLUSH_DIARY_REPLY)
  },
  loadDiaryReplyHistory ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, state.loadDiaryReplyHistoryQuery, data, {
      coachId: user.id
    })
    commit(LOAD_DIARY_REPLY_HISTORY, _data)
    fetch.get(API_DIARY_REPLY_HISTORY, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_DIARY_REPLY_HISTORY_SUCCESS, res.data)
      } else {
        commit(LOAD_DIARY_REPLY_HISTORY_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_DIARY_REPLY_HISTORY_FAIL, '加载失败, 请重试')
    })
  },
  loadDiaryReply ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    commit(LOAD_DIARY_REPLY)
    
    fetch.get(API_DIARY_REPLY_DETAIL, data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_DIARY_REPLY_SUCCESS, res.data)
      } else {
        commit(LOAD_DIARY_REPLY_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_DIARY_REPLY_FAIL, '获取失败, 请重试')
    })
  },
  submitDiaryReply ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, data, {
      coachId: user.id
    })
    commit(SUBMIT_DIARY_REPLY)
    fetch.post(API_DIARY_REPLY_SUBMIT, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(SUBMIT_DIARY_REPLY_SUCCESS, res.data)
      } else {
        commit(SUBMIT_DIARY_REPLY_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(SUBMIT_DIARY_REPLY_FAIL, '提交失败, 请重试')
    })
  }
}

const mutations = {
  [FLUSH_DIARY_REPLY] (state) {
    state.diaryReply = null
  },
  [LOAD_DIARY_REPLY_HISTORY] (state, data) {
    state.loadDiaryReplyHistoryStatus = PENDING
    state.loadDiaryReplyHistoryMessage = ''
    state.loadDiaryReplyHistoryQuery = data
  },
  [LOAD_DIARY_REPLY_HISTORY_FAIL] (state, message) {
    state.loadDiaryReplyHistoryStatus = FAIL
    state.loadDiaryReplyHistoryMessage = message
  },
  [LOAD_DIARY_REPLY_HISTORY_SUCCESS] (state, data) {
    let list = [...state.diaryReplyList]
    let filted_list = data.data.filter((item) => {
      let existed = list.find((_item) => {
        return item.diary.id === _item.diary.id
      })
      return !existed
    })
    
    let _list = filted_list.concat(list)
    state.diaryReplyList = _list
    state.loadDiaryReplyHistoryStatus = SUCCESS
    state.loadDiaryReplyHistoryTotal = data.recordsTotal
  },
  [LOAD_DIARY_REPLY] (state) {
    state.loadDiaryReplyStatus = PENDING
  },
  [LOAD_DIARY_REPLY_FAIL] (state, message) {
    state.loadDiaryReplyMessage = message
    state.loadDiaryReplyStatus = FAIL
  },
  [LOAD_DIARY_REPLY_SUCCESS] (state, data) {
    state.loadDiaryReplyStatus = SUCCESS
    state.diaryReply = data
  },
  [SUBMIT_DIARY_REPLY] (state) {
    state.submitDiaryReplyStatus = PENDING
  },
  [SUBMIT_DIARY_REPLY_FAIL] (state, message) {
    state.submitDiaryReplyStatus = FAIL
    state.submitDiaryReplyMessage = message
  },
  [SUBMIT_DIARY_REPLY_SUCCESS] (state, data) {
    state.submitDiaryReplyStatus = SUCCESS
    state.diaryReply = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}