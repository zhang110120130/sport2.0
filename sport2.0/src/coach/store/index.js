/**
 * 教练员 store
 */
import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import homepage from './modules/homepage'
import info from './modules/info'
import reply from './modules/reply'
import plan from './modules/plan'
import message from './modules/message'

import avatar from 'common/stores/avatar'
import feedback from 'common/stores/feedback'
import login from 'common/stores/login'
import logout from 'common/stores/logout'
import password from 'common/stores/password'
import profile from 'common/stores/profile'
import status from 'common/stores/status'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    avatar,
    feedback,
    login,
    logout,
    password,
    profile,
    status,
    homepage,
    info,
    reply,
    plan,
    message,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})

export default store