import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import {
  LOAD_PROFILE, LOAD_PROFILE_SUCCESS, LOAD_PROFILE_FAIL,
  LOAD_USER_STATUS, LOAD_USER_STATUS_SUCCESS, LOAD_USER_STATUS_FAIL,
  LOAD_DIARY_REPLIED, LOAD_DIARY_REPLIED_SUCCESS, LOAD_DIARY_REPLIED_FAIL,
  LOAD_HOME_BACKGROUND, LOAD_HOME_BACKGROUND_SUCCESS, LOAD_HOME_BACKGROUND_FAIL
} from 'player/constants/action-types'
import { API_PROFILE, API_USER_STATUS, 
         API_DIARY_REPLIED, API_HOME_BACKGROUND } from 'player/constants/api'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'

const state = {
  loadDiaryRepliedStatus: DEFAULT,
  diaryReplied: null,
  background: null
}

const getters = createGetters(state)

const actions = {
  loadBackground ({commit, state}) {
    const user = StorageMaster.getItem('user')
    fetch.get(API_HOME_BACKGROUND, {playerId: user.id}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_HOME_BACKGROUND_SUCCESS, res.data)
      } else {
        commit(LOAD_HOME_BACKGROUND_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  },
  loadDiaryReplied ({commit, state}) {
    const user = StorageMaster.getItem('user')
    commit(LOAD_DIARY_REPLIED, null)
    fetch.get(API_DIARY_REPLIED, {playerId: user.id}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_DIARY_REPLIED_SUCCESS, res.data)
      } else {
        commit(LOAD_DIARY_REPLIED_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  }
}

const mutations = {
  [LOAD_HOME_BACKGROUND_SUCCESS] (state, background) {
    state.background = background
  },
  [LOAD_DIARY_REPLIED] (state) {
    state.loadDiaryRepliedStatus = PENDING
  },
  [LOAD_DIARY_REPLIED_SUCCESS] (state, replied) {
    state.diaryReplied = replied
    state.loadDiaryRepliedStatus = SUCCESS
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}