import Vue from 'vue'
import routes from 'coach/routes'
import App from 'coach/views/App'
import Router from 'vue-router'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes
})

let VueApp = new Vue({
  router,
  components: { App },
  template: '<App />',
})

VueApp.$mount('#app')