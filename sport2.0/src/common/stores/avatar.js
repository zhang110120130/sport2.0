import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { CHANGE_AVATAR, CHANGE_AVATAR_FAIL, CHANGE_AVATAR_SUCCESS } from 'common/constants/action-types'
import { API_CHANGE_AVATAR } from 'common/constants/api'
import { StorageMaster, Storage } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'

const state = {
  changeAvatarStatus: DEFAULT,
  changeAvatarMessage: '',
  avatar: ''
}

const getters = createGetters(state)

const actions = {
  changeAvatar ({commit, state}, avatar) {
    const setting = StorageMaster.getItem('setting')
    const user = StorageMaster.getItem('user')
    let submitData = {
      [`${setting.role}Id`]: user.id,
      avatar: avatar
    }

    commit(CHANGE_AVATAR, avatar)
    fetch.post(API_CHANGE_AVATAR, submitData, {role: setting.role}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(CHANGE_AVATAR_SUCCESS, avatar, res)
      } else {
        commit(CHANGE_AVATAR_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  }
}

const mutations = {
  [CHANGE_AVATAR] (state, avatar) {
    state.changeAvatarStatus = PENDING
  },
  [CHANGE_AVATAR_FAIL] (state, message) {
    state.changeAvatarStatus = FAIL
    state.changeAvatarMessage = message
  },
  [CHANGE_AVATAR_SUCCESS] (state, avatar) {
    state.changeAvatarStatus = SUCCESS
    state.avatar = avatar

    try {
      let user = StorageMaster.getItem('user')
      let profile = StorageMaster.getItem('profile')
      user.avatar = avatar
      profile.avatar = avatar
      StorageMaster.setItem('user', user)
      StorageMaster.setItem('profile', profile)
    } catch (e) {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}