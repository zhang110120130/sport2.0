/**
 * 加载用户状态
 */
import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { LOAD_USER_STATUS, LOAD_USER_STATUS_FAIL, 
         LOAD_USER_STATUS_SUCCESS } from 'common/constants/action-types'
import { API_USER_STATUS } from 'common/constants/api'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'


const state = {
  loadUserStatusStatus: DEFAULT,
  loadUserStatusMessage: '',
  userStatus: null,
}

const getters = createGetters(state)

const actions = {
  loadUserStatus ({commit, state}) {
    const user = StorageMaster.getItem('user')
    const setting = StorageMaster.getItem('setting')
    let data = {
      [`${setting.role}Id`]: user.id
    }
    
    fetch.get(API_USER_STATUS, data, {role: setting.role}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_USER_STATUS_SUCCESS, res.data)
      } else {
        commit(LOAD_USER_STATUS_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_USER_STATUS_FAIL, '加载失败, 请重试')
    })
  }
}

const mutations = {
  [LOAD_USER_STATUS] (state) {
    state.loadUserStatusStatus = PENDING
  },
  [LOAD_USER_STATUS_FAIL] (state, message) {
    state.loadUserStatusStatus = FAIL
    state.loadUserStatusMessage = message
  },
  [LOAD_USER_STATUS_SUCCESS] (state, data) {
    state.loadUserStatusStatus = SUCCESS
    state.userStatus = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}