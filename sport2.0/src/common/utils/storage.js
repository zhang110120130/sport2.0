export const Storage = window.localStorage

/**
 * user:
 * {
 *  gender: 'male/female',
 *  role: 'player/coach'
 * }
 */


export const StorageMaster = {
  setItem (key, value) {
    let _value = typeof(value) === 'string' ? value : JSON.stringify(value)
    console.log(`set ${key}: `, _value)
    return Storage.setItem(key, _value)
  },
  getItem (key) {
    let value = Storage.getItem(key)
    try {
      return JSON.parse(value)
    } catch (e) {
      return value
    }
  },
  removeItem (key) {
    return Storage.removeItem(key)
  },
  clear () {
    return Storage.clear()
  }
}

// 初始化缓存数据
// 通常会初始化用户登录状态和用户身份
export const StorageInit = (data) => {
  for (let key in data) {
    StorageMaster.setItem(key, data[key])
  }
}
