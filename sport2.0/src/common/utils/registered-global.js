/**
 * 注册一些常量到 Vue 全局中
 */

import Vue from 'vue'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'

Vue.prototype.GLOBAL = {}

export default function registeredGlobal() {
  Vue.prototype.GLOBAL.ASYNC_SUCCESS = SUCCESS
  Vue.prototype.GLOBAL.ASYNC_PENDING = PENDING
  Vue.prototype.GLOBAL.ASYNC_FAIL = FAIL
  Vue.prototype.GLOBAL.ASYNC_DEFAULT = DEFAULT
}

