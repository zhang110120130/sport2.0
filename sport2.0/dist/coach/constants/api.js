/**
 * 教练员
 */

// 上传文件
export const API_UPLOAD = '/v1/file/uploadSingle'
// 登录页面背景图
export const API_LOGIN_BACKGROUND = '/v1/coach/login/background'
// 首页背景图
export const API_HOME_BACKGROUND = '/v1/coach/home/background'

// 首页日记批复
export const API_HOME_DIARY = '/v1/coach/home/diary'
// 首页训练计划
export const API_HOME_PLAN = '/v1/coach/home/plan'

// 发布情报
export const API_TRAINING_INFO_SUBMIT = '/v1/coach/training/info/submit'
// 获取情报历史
export const API_TRAINING_INFO_HISTORY = '/v1/coach/training/info/history'
// 获取情报详情
export const API_TRAINING_INFO_DETAIL = '/v1/coach/training/info/detail'

// 获取计划历史
export const API_TRAINING_PLAN_HISTORY = '/v1/coach/training/plan/history'
// 获取计划详情
export const API_TRAINING_PLAN_DETAIL = '/v1/coach/training/plan/detail'
// 发布计划
export const API_TRAINING_PLAN_SUBMIT = '/v1/coach/training/plan/submit'
// 更新计划
export const API_TRAINING_PLAN_UPDATE = '/v1/coach/training/plan/update'

// 提交日记批示
export const API_DIARY_REPLY_SUBMIT = '/v1/coach/diary/reply'
// 获取日记批复历史
export const API_DIARY_REPLY_HISTORY = '/v1/coach/diary/history'
// 获取日记批复详情
export const API_DIARY_REPLY_DETAIL = '/v1/player/diary/detail'


// 获取未读消息数字
export const API_UNREAD_MESSAGE = '/v1/coach/message/unread'
export const API_MESSAGE_LIST = '/v1/coach/message/list'
export const API_MESSAGE_DETAIL = '/v1/coach/message/detail'

// 更换头像
export const API_AVATAR = '/v1/coach/avatar'
// 修改密码
export const API_PASSWORD = '/v1/coach/password'
// 反馈信息
export const API_FEEDBACK = '/v1/coach/feedback'
