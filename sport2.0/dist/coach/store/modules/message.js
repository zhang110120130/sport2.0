import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { LOAD_MESSAGE_LIST, LOAD_MESSAGE_LIST_FAIL, LOAD_MESSAGE_LIST_SUCCESS,
         LOAD_UNRED_MESSAGE, LOAD_UNRED_MESSAGE_FAIL, LOAD_UNRED_MESSAGE_SUCCESS,
         LOAD_MESSAGE_DETAIL, LOAD_MESSAGE_DETAIL_FAIL, LOAD_MESSAGE_DETAIL_SUCCESS } from 'coach/constants/action-types'
import { API_FEEDBACK, API_MESSAGE_LIST, API_MESSAGE_DETAIL, API_UNREAD_MESSAGE } from 'coach/constants/api'
import createGetters from 'common/utils/create-getters'
import { StorageMaster } from 'common/utils/storage'

const state = {
  loadMessageListStatus: DEFAULT,
  messageListQuery: {
    pageNo: 1,
    pageSize: 10
  },
  messageList: [],
  loadMessageListMessage: '',
  loadUnreadStatus: DEFAULT,
  unreadCount: null,
  loadUnreadMessage: '',
  loadMessageStatus: DEFAULT,
  message: null,
  loadMessageDetailMessage: ''
}

const getters = createGetters(state)

const actions = {
  loadMessageList ({commit, state}, query) {
    const user = StorageMaster.getItem('user')
    let _query = Object.assign({}, query, {
      coachId: user.id
    })
    commit(LOAD_MESSAGE_LIST, _query)
    fetch.get(API_MESSAGE_LIST, _query).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_MESSAGE_LIST_SUCCESS, res.data)
      } else {
        commit(LOAD_MESSAGE_LIST_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_MESSAGE_LIST_FAIL, err)
    })
  },
  loadMessageDetail ({commit, state}, messageId) {
    commit(LOAD_MESSAGE_DETAIL, messageId)
    fetch.get(API_MESSAGE_DETAIL, { messageId }).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_MESSAGE_DETAIL_SUCCESS, res.data)
      } else {
        commit(LOAD_MESSAGE_DETAIL_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_MESSAGE_DETAIL_FAIL, err)
    })
  },
  loadUnreadMessage ({commit, state}) {
    const user = StorageMaster.getItem('user')
    let coachId = user.id
    commit(LOAD_UNRED_MESSAGE)
    fetch.get(API_UNREAD_MESSAGE, { coachId }).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_UNRED_MESSAGE_SUCCESS, res.data.count)
      } else {
        commit(LOAD_UNRED_MESSAGE_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_UNRED_MESSAGE_FAIL, err)
    })
  }
}

const mutations = {
  [LOAD_MESSAGE_LIST] (state, query) {
    state.loadMessageListStatus = PENDING
    state.messageListQuery = query
  },
  [LOAD_MESSAGE_LIST_SUCCESS] (state, data) {
    let list = [...state.messageList]
    let filted_list = data.content.filter((item) => {
      let existed = list.find((_item) => {
        return item.id == _item.id
      })
      return !existed
    })
    let _list = filted_list.concat(list)
    state.messageList = _list
    state.loadMessageListStatus = SUCCESS
    
  },
  [LOAD_MESSAGE_LIST_FAIL] (state, message) {
    state.loadMessageListStatus = FAIL
    state.loadMessageListMessage = message
  },
  [LOAD_UNRED_MESSAGE] (state) {
    state.loadUnreadStatus = PENDING
  },
  [LOAD_UNRED_MESSAGE_SUCCESS] (state, count) {
    state.loadUnreadStatus = SUCCESS
    state.unreadCount = count
  },
  [LOAD_UNRED_MESSAGE_FAIL] (state) {
    state.loadUnreadStatus = FAIL
  },
  [LOAD_MESSAGE_DETAIL] (state) {
    state.loadMessageStatus = PENDING
  },
  [LOAD_MESSAGE_DETAIL_SUCCESS] (state, data) {
    state.loadMessageStatus = SUCCESS
    state.message = data
  },
  [LOAD_MESSAGE_DETAIL_FAIL] (state, message) {
    state.loadMessageStatus = FAIL
    state.loadMessageDetailMessage = message
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}