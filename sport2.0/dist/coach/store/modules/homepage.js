import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { LOAD_HOME_BACKGROUND, LOAD_HOME_BACKGROUND_FAIL, 
         LOAD_HOME_BACKGROUND_SUCCESS, LOAD_DIARY_REPLIED,
         LOAD_DIARY_REPLIED_FAIL, LOAD_DIARY_REPLIED_SUCCESS,
         LOAD_HOME_PLAN, LOAD_HOME_PLAN_FAIL,
         LOAD_HOME_PLAN_SUCCESS } from 'coach/constants/action-types'
import { API_HOME_BACKGROUND, API_HOME_DIARY, API_HOME_PLAN } from 'coach/constants/api'
import { StorageMaster } from 'common/utils/storage'


const state = {
  loadDiaryRepliedStatus: DEFAULT,
  loadRiaryRepliedMessage: '',
  diaryReplied: null,
  loadHomePlanStatus: DEFAULT,
  loadHomePlanMessage: '',
  homePlan: null,
  background: null
}

const getters = {
  loadDiaryRepliedStatus: (state) => {
    return state.loadDiaryRepliedStatus
  },
  diaryReplied: (state) => {
    return state.diaryReplied
  },
  loadHomePlanStatus: (state) => {
    return state.loadHomePlanStatus
  },
  homePlan: (state) => {
    return state.homePlan
  },
  background: (state) => {
    return state.background
  }
}

const actions = {
  loadBackground ({commit, state}) {
    const user = StorageMaster.getItem('user')
    fetch.get(API_HOME_BACKGROUND,  {coachId: user.id}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_HOME_BACKGROUND_SUCCESS, res.data)
      } else {
        commit(LOAD_HOME_BACKGROUND_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_HOME_BACKGROUND_FAIL, '加载背景图失败')
    })
  },
  loadDiaryReplied ({commit, state}) {
    const user = StorageMaster.getItem('user')
    commit(LOAD_DIARY_REPLIED, null)
    fetch.get(API_HOME_DIARY, {coachId: user.id}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_DIARY_REPLIED_SUCCESS, res.data)
      } else {
        commit(LOAD_DIARY_REPLIED_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  },
  loadHomePlan ({commit, state}) {
    const user = StorageMaster.getItem('user')
    commit(LOAD_HOME_PLAN, null)
    fetch.get(API_HOME_PLAN, {coachId: user.id}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_HOME_PLAN_SUCCESS, res.data)
      } else {
        commit(LOAD_HOME_PLAN_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_HOME_PLAN)
    })
  }
}

const mutations = {
  [LOAD_HOME_BACKGROUND_SUCCESS] (state, data) {
    state.background = data
  },
  [LOAD_DIARY_REPLIED] (state) {
    state.loadDiaryRepliedStatus = PENDING
  },
  [LOAD_DIARY_REPLIED_FAIL] (state, message) {
    state.loadDiaryRepliedStatus = FAIL
    state.loadRiaryRepliedMessage = message
  },
  [LOAD_DIARY_REPLIED_SUCCESS] (state, data) {
    state.loadDiaryRepliedStatus = SUCCESS
    state.diaryReplied = data
  },
  [LOAD_HOME_PLAN] (state) {
    state.loadDiaryRepliedStatus = PENDING
  },
  [LOAD_HOME_PLAN_FAIL] (state, message) {
    state.loadHomePlanStatus = FAIL
    state.loadHomePlanMessage = message
  },
  [LOAD_HOME_PLAN_SUCCESS] (state, data) {
    state.loadHomePlanStatus = SUCCESS
    state.homePlan = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}