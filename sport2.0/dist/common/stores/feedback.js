import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { SUBMIT_FEEDBACK, SUBMIT_FEEDBACK_FAIL, SUBMIT_FEEDBACK_SUCCESS } from 'common/constants/action-types'
import { API_SUBMIT_FEEDBACK } from 'common/constants/api'
import { StorageMaster } from 'common/utils/storage'
import { SUBMIT_DIARY_SUCCESS } from 'player/constants/action-types';
import createGetters from 'common/utils/create-getters'

const state = {
  submitFeedbackStatus: PENDING,
  submitFeedbackMessage: ''
}

const getters = createGetters(state)

const actions = {
  submitFeedback ({commit}, data) {
    commit(SUBMIT_FEEDBACK)
    let setting = StorageMaster.getItem('setting')
    let user = StorageMaster.getItem('user')
    let _data = Object.assign({}, data, {
      [`${setting.role}Id`]: user.id
    })
    fetch.post(API_SUBMIT_FEEDBACK, _data, { role: setting.role }).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(SUBMIT_FEEDBACK_SUCCESS, res)
      } else {
        commit(SUBMIT_FEEDBACK_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  }
}

const mutations = {
  [SUBMIT_FEEDBACK] (state) {
    state.submitFeedbackStatus = PENDING
    state.submitFeedbackMessage = ''
  },
  [SUBMIT_FEEDBACK_FAIL] (state, message) {
    state.submitFeedbackStatus = FAIL
    state.submitFeedbackMessage = message
  },
  [SUBMIT_FEEDBACK_SUCCESS] (state) {
    state.submitFeedbackStatus = SUCCESS
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}