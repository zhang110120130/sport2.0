/**
 * 加载 profile
 */

import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { LOAD_PROFILE, LOAD_PROFILE_FAIL, 
         LOAD_PROFILE_SUCCESS, UPDATE_PROFILE,
         UPDATE_PROFILE_FAIL, UPDATE_PROFILE_SUCCESS } from 'common/constants/action-types'
import { API_PROFILE } from 'common/constants/api'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'

const state = {
  loadProfileStatus: DEFAULT,
  loadProfileMessage: '',
  updateProfileStatus: DEFAULT,
  updateProfileMessage: '',
  profile: null
}

const getters = createGetters(state)

const actions = {
  loadProfile ({commit, state}) {
    commit(LOAD_PROFILE)
    const user = StorageMaster.getItem('user')
    const setting = StorageMaster.getItem('setting')
    
    let data = {
      [`${setting.role}Id`]: user.id
    }
    fetch.get(API_PROFILE, data, { role: setting.role }).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_PROFILE_SUCCESS, res.data)
      } else {
        commit(LOAD_PROFILE_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_PROFILE_FAIL, '加载个人信息失败, 请重试')
    })
  },
  updateProfile({commit, state}, submitData) {
    commit(UPDATE_PROFILE)
    const user = StorageMaster.getItem('user')
    const setting = StorageMaster.getItem('setting')
    let data = Object.assign({}, submitData,{
      [`${setting.role}Id`]: user.id
    })
    fetch.post(API_PROFILE, data, {role: setting.role}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(UPDATE_PROFILE_SUCCESS, res.data)
      } else {
        commit(UPDATE_PROFILE_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(UPDATE_PROFILE_FAIL, '网络错误, 请重试')
    })
  }
}

const mutations = {
  [LOAD_PROFILE] (state) {
    state.loadProfileStatus = PENDING
  },
  [LOAD_PROFILE_SUCCESS] (state, profile) {
    state.loadProfileStatus = SUCCESS
    state.profile = profile
    StorageMaster.setItem('profile', profile)
  },
  [LOAD_PROFILE_FAIL] (state, message) {
    state.loadProfileStatus = FAIL
    state.loadProfileMessage = message
  },
  [UPDATE_PROFILE] (state) {
    state.updateProfileStatus = PENDING
  },
  [UPDATE_PROFILE_FAIL] (state, message) {
    state.updateProfileStatus = FAIL
    state.updateProfileMessage = message
  },
  [UPDATE_PROFILE_SUCCESS] (state, data) {
    state.updateProfileStatus = SUCCESS
    state.profile = data
    const user = StorageMaster.getItem('user')
    let _user = Object.assign({}, user, data)
    StorageMaster.setItem('profileStatus', 'edited')
    StorageMaster.setItem('user', _user)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}