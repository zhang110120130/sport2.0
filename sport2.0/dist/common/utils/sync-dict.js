/**
 * 同步常量 dict
 */

import fetch from './fetch'
import { StorageMaster, Storage } from './storage'
import { API_DICT, API_DICT_CATEGORY } from 'common/constants/api'
import { DICT_LIST } from 'common/constants/const'

// get dict, 并更新到 storage 中
export default function syncDict(success, fail, dict_list=DICT_LIST) {
  let promise_list = dict_list.map((name) => {
    return new Promise((resolve, reject) => {
      fetch.get(API_DICT, null, { name }).then((res) => {
        if (parseInt(res.code, 10) === 1) {
          StorageMaster.setItem(name, res.data)
          resolve(res.data)
        } else {
          reject(res)
        }
      }).catch((err) => {
        console.error(err)
        reject(err)
      })
    })
  })
  Promise.all(promise_list).then(() => {
    StorageMaster.setItem('dict_sync_status', {
      status: 'success',
      list: dict_list,
      date: new Date().toString()
    })
    success && success()
  }).catch((err) => {
    StorageMaster.setItem('dict_sync_status', {
      status: 'fail',
      list: dict_list,
      date: new Date().toString()
    })
    fail && fail()
  })
}

// 同步运动大项和小项
export function syncSportCategory (id=0, name='sportCategoryRoot') {
  return new Promise((resolve, reject) => {
    let cache = StorageMaster.getItem(name)
    if (cache) {
      resolve(cache)
    } else {
      fetch.get(API_DICT_CATEGORY, null, {id}).then((res) => {
        if (parseInt(res.code, 10) === 1) {
          StorageMaster.setItem(name, res.data)
          resolve(res.data)
        } else {
          reject(res.message)
        }
      }).catch((err) => {
        reject(res.err)
      })
    }
  })
}