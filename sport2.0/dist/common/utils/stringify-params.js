/**
 * 将 ajax post 的 params 中的 value 都 stringify 一下
 */

export default (params) => {
  let _params = Object.assign({}, params)

  for (let k in _params) {
    if (_params.hasOwnProperty(k)) {
      let v = _params[k]
      let type = Object.prototype.toString.call(v)
      if (/\[object Array\]/gi.test(type) || /\[object Object\]/gi.test(type)) {
        _params[k] = JSON.stringify(v)
      }
    }
  }

  return _params
}