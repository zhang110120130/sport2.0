// helpers

export function list2dict(list) {
  return list.reduce((r, item) => {
    r[item.value] = item.name
    return r
  }, {})
}

/**
 * transform name like as: trainingVolume to TRANING_VOLUME
 */
export function trans2const(name, pre_fix, after_fix) {
  let reg = /[A-Z]/g
  let r = name.replace(reg, ($1) => {
    return `_${$1}`
  }).split('_').map((p) => {
    return p.toUpperCase()
  })
  return `${pre_fix}${r.join('_')}${after_fix}`
}