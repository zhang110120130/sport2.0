/**
 * need cordova-plugin-file-transfer
 */

let DEFAULT_CONFIG = {
  create: false
}
export default (fileURI, config) => {
  let _config = Object.assign({}, DEFAULT_CONFIG, config)

  return new Promise((resolve, reject) => {
    if (!requestFileSystem) {
      alert('requestFileSystem 不存在!')
      reject('requestFileSystem 不存在!')
      return false
    }
    if (!LocalFileSystem) {
      alert('LocalFileSystem 不存在!')
      reject('LocalFileSystem 不存在!')
      return false
    }

    requestFileSystem(LocalFileSystem.TEMPORARY, 0, (fs) => {
      fs.root.getFile(fileURI, _config, (fileEntry) => {
        alert('getFile success')
        resolve(fileEntry)
      }, (err) => {
        alert('getFile error')
        reject(err)
      })
    }, (err) => {
      reject('create file failed')
    })
  })
}
