/**
 * 检测当前时间是否可以测量脉搏
 */
import moment from 'moment'

let m = moment()

export const MORNIG_PULSE_TIME_START = '05:00:00'
export const MORNIG_PULSE_TIME_END = '10:00:00'
export const NIGHT_PULSE_TIME_START = '19:00'
export const NIGHT_PULSE_TIME_END = '24:00'

export function inMorning() {
  let now = m.format('HH:MM:SS')
  return now >= MORNIG_PULSE_TIME_START && now <= MORNIG_PULSE_TIME_END
}

export function inNight() {
  let now = m.format('HH:MM:SS')
  return now >= NIGHT_PULSE_TIME_START && now <= NIGHT_PULSE_TIME_END
}

export function isCanPulse() {
  return inMorning() || inNight()
}