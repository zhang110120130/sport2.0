/**
 * 提供 cordova 调取相册/相机 的方法
 */


export function takePhoto (config) {
  let destinationType = navigator.camera.DestinationType

  let defaultConfig = {
    quality: 90,
    destinationType: destinationType.FILE_URI
  }
  let _config = Object.assign({}, defaultConfig, config)

  return new Promise((resolve, reject) => {
    navigator.camera.getPicture(resolve, reject, _config)
  })
}


function AndroidGetPhoto(config) {
  return new Promise((resolve, reject) => {
    if (!cordova) {
      reject('找不到 cordova 对象')
      return false
    }
    let permissions = cordova.plugins.permissions
    if (!permissions) {
      reject('找不到 permissions 对象')
      return false
    }
    let permissionList = [
      permissions.WRITE_EXTERNAL_STORAGE,
      permissions.CAMERA
    ]
    permissions.requestPermissions(permissionList, function(status) {
      if (status.hasPermission) {
        ImagePicker.getPictures(resolve, reject, config)
      } else {
        reject('请在[设置]中为本 app 设置权限')
      }
    }, function (err) {
      reject('获取 permissions 出错')
    })
  })
}

export function getPhoto (config) {
  let defaultConfig = {
    maximumImagesCount: 1,
    width: 1920,
    height: 1440,
    quality: 100
  }
  let _config = Object.assign({}, defaultConfig, config)
  
  var device = window.device || null

  // if (!device) {
  //   alert('检测不到设备信息');
  //   return false;
  // }
  if (device && /Android/gi.test(device.platform)) {
    return AndroidGetPhoto(_config)
  }


  return new Promise((resolve, reject) => {
    try {
      ImagePicker.getPictures(resolve, reject, _config)
    } catch (e) {
      reject(e)
    }
  })

}