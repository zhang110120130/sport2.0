/**
 * 检查用户权限
 */

import { StorageMaster } from 'common/utils/storage'

let login_path = '/login'
let profile_edit_path = '/profile/edit'

export function routerLoginCheck(router) {
  router.beforeEach((to, from, next) => {
    let user = StorageMaster.getItem('user')
    let profileStatus = StorageMaster.getItem('profileStatus')

    let { path } = to
    if (!user && path !== login_path) {
      router.push({path: '/login'})
    } else if (user && user.first_login && profileStatus !== 'edited' && path !== profile_edit_path) {
      router.push({path: '/profile/edit'})
    }
    next()
  })
}