/**
 * common api
 */

export const API_CHANGE_PASSWORD = '/v1/:role/password'
export const API_SUBMIT_FEEDBACK = '/v1/:role/feedback'
export const API_CHANGE_AVATAR = '/v1/:role/avatar'
export const API_LOGIN = '/v1/:role/login'
export const API_LOGOUT = '/v1/:role/logout'
export const API_LOGIN_BACKGROUND = '/v1/:role/login/background'

// profile
export const API_PROFILE = '/v1/:role/profile'
export const API_USER_STATUS = '/v1/:role/status'

// dict api
export const API_DICT = '/v1/dict/:name'
// 教练级别
export const API_DICT_COACH_LEVEL = '/v1/dict/coachLevel'
// 完成情况
export const API_DICT_COMPLETE_STATUS = '/v1/dict/completeStatus'
// 效果评估
export const API_DICT_EVALUATION_STATUS = '/v1/dict/evaluationStatus'
// 受伤部位
export const API_DICT_INJURY_PART = '/v1/dict/injuryPart'
// 心情
export const API_DICT_MOOD_STATUS = '/v1/dict/moodStatus'
// 经期
export const API_DICT_PERIOD_STATUS = '/v1/dict/periodStatus'
// 脉搏
export const API_DICT_PULSE_TYPE = '/v1/dict/pulseType'
// 资源类型
export const API_DICT_RESOURCE_TYPE = '/v1/dict/resourceType'
// 队伍级别
export const API_DICT_TEAM_LEVEL = '/v1/dict/teamLevel'
// 训练强度
export const API_DICT_TRAINING_INTENSITY = '/v1/dict/trainingIntensity'
// 训练量
export const API_DICT_TRAINING_VOLUME = '/v1/dict/trainingVolume'
// 训练内容
export const API_DICT_TRAINING_CONRENT = '/v1/dict/trainingContent'
// 恢复方法
export const API_DICT_RECOVER_METHOD = '/v1/dict/trainingRecoveryMethod'
// 运动大项/小项
export const API_DICT_CATEGORY = '/v1/dict/sportCategory/:id/children'
// 性别
export const API_DICT_GENDER = '/v1/dict/gender'
// 血型
export const API_DICT_BLOODTYPE = '/v1/dict/bloodType'
// 时间段
export const API_DICT_TIMEPERIOD = '/v1/dict/timePeriod'