import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { SUBMIT_DIARY, SUBMIT_DIARY_SUCCESS, SUBMIT_DIARY_FAIL,
         LOAD_DIARY, LOAD_DIARY_SUCCESS, LOAD_DIARY_FAIL, 
         SAVE_DIARY, SAVE_DIARY_FAIL, SAVE_DIARY_SUCCESS,
         FLUSH_DIARY_CACHE } from 'player/constants/action-types'
import { API_DIARY_DETAIL, API_DIARY_SUBMIT, DIRAY_STEP_API_ALIAS } from 'player/constants/api'
import { StorageMaster } from 'common/utils/storage';
import createGetters from 'common/utils/create-getters'

const state = {
  diaryLoadStatus: DEFAULT,
  diaryLoadMessage: '',
  diarySubmitStatus: DEFAULT,
  diarySubmitMessage: '',
  diarySaveStatus: DEFAULT,
  diarySaveMessage: '',
  // 单个日记数据
  diary: null
}

const getters = createGetters(state)

const actions = {
  flushDiaryCache ({commit, state}) {
    commit(FLUSH_DIARY_CACHE)
  },
  saveDiaryInfo({commit, state}, {type, data}) {
    let api = DIRAY_STEP_API_ALIAS[type]
    if (api) {
      commit(SAVE_DIARY, {type, data})
      fetch.post(api, data, null).then((res) => {
        if (parseInt(res.code, 10) === 1) {
          commit(SAVE_DIARY_SUCCESS, res.data, type)
        } else {
          let message = res.message
          if (parseInt(res.code, 10) === 2) {
            message = '当日日记已经提交, 请勿重复提交'
          }
          commit(SAVE_DIARY_FAIL, message)
        }
      }).catch((err) => {
        commit(SAVE_DIARY_FAIL, '网络错误, 提交失败, 请重试')
      })
    } else {
      let message = `${type} 找不到对应的 api`
      console.error(message)
      commit(SAVE_DIARY_FAIL, message)
    }
  },
  submitDiary ({commit, state}, data) {
    commit(SUBMIT_DIARY, data)
    fetch.post(API_DIARY_SUBMIT, data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(SUBMIT_DIARY_SUCCESS, res.data)
      } else {
        let message = res.message
        if (parseInt(res.code, 10) === 2) {
          message = '当日日记已经提交, 请勿重复提交'
        }
        commit(SUBMIT_DIARY_FAIL, message)
      }
    }).catch((err) => {
      console.error(err)
      commit(SUBMIT_DIARY_FAIL, '网络错误, 请重试')
    })
  },
  loadDiary ({commit, state}, data) {
    commit(LOAD_DIARY)
    fetch.get(API_DIARY_DETAIL, data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_DIARY_SUCCESS, res.data)
      } else {
        commit(LOAD_DIARY_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  }
}

const mutations = {
  [FLUSH_DIARY_CACHE] (state) {
    state.diary = null
  },
  [SAVE_DIARY] (state) {
    state.diarySaveMessage = ''
    state.diarySaveStatus = PENDING
  },
  [SAVE_DIARY_SUCCESS] (state, diary) {
    console.log('SAVE_DIARY_SUCCESS')
    state.diarySaveStatus = SUCCESS
    if (diary) {
      state.diary = diary
    }
  },
  [SAVE_DIARY_FAIL] (state, message) {
    state.diarySaveStatus = FAIL
    state.diarySaveMessage = message
  },
  [SUBMIT_DIARY] (state) {
    state.diarySubmitStatus = PENDING
    state.diarySubmitMessage = ''
  },
  [SUBMIT_DIARY_FAIL] (state, message) {
    state.diarySubmitStatus = FAIL
    state.diarySubmitMessage = message
  },
  [SUBMIT_DIARY_SUCCESS] (state, message) {
    state.diarySubmitStatus = SUCCESS
  },
  [LOAD_DIARY] (state) {
    state.diaryLoadStatus = PENDING
    state.diaryLoadMessage = ''
  },
  [LOAD_DIARY_FAIL] (state, message) {
    state.diaryLoadStatus = FAIL
    state.diaryLoadMessage = message
  },
  [LOAD_DIARY_SUCCESS] (state, diary) {
    state.diaryLoadStatus = SUCCESS
    state.diary = diary
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}