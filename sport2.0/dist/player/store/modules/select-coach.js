import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { LOAD_COACH_LIST, LOAD_COACH_LIST_FAIL, 
         LOAD_COACH_LIST_SUCCESS, SAVE_COACH,
         SAVE_COACH_FAIL, SAVE_COACH_SUCCESS } from 'player/constants/action-types'
import { API_COACH_LIST, API_CHOOSE_COACH } from 'player/constants/api'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'

const state = {
  loadCoachListStatus: DEFAULT,
  loadCoachListMessage: '',
  coachList: [],
  
  saveCoachStatus: DEFAULT,
  saveCoachMessage: ''
}

const getters = createGetters(state)

const actions = {
  loadCoachList ({commit, state}) {
    fetch.get(API_COACH_LIST).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_COACH_LIST_SUCCESS, res.data)
      } else {
        commit(LOAD_COACH_LIST_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_COACH_LIST_FAIL, '网络错误, 请重试')
    })
  },
  saveCoach({commit, state}, coachId) {
    const user = StorageMaster.getItem('user')
    let data = {
      playerId: user.id,
      coachId: coachId
    }
    commit(SAVE_COACH)
    fetch.post(API_CHOOSE_COACH, data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(SAVE_COACH_SUCCESS, res.data)
      } else {
        commit(SAVE_COACH_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(SAVE_COACH_FAIL, '网络错误, 请重试')
    })
  }
}

const mutations = {
  [LOAD_COACH_LIST] (state) {
    state.loadCoachListStatus = PENDING
  },
  [LOAD_COACH_LIST_FAIL] (state, message) {
    state.loadCoachListStatus = FAIL
    state.loadCoachListMessage = message
  },
  [LOAD_COACH_LIST_SUCCESS] (state, data) {
    state.loadCoachListStatus = SUCCESS
    state.coachList = data
  },
  [SAVE_COACH] (state) {
    state.saveCoachStatus = PENDING
  },
  [SAVE_COACH_FAIL] (state, message) {
    state.saveCoachStatus = FAIL
    state.saveCoachMessage = message
  },
  [SAVE_COACH_SUCCESS] (state) {
    state.saveCoachStatus = SUCCESS
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}