/**
 * api 列表
 */

// 上传文件
export const API_UPLOAD = '/v1/file/uploadSingle'
// 获取登录页背景资源
export const API_LOGIN_BACKGROUND = '/v1/player/login/background'
// 获取首页背景资源
export const API_HOME_BACKGROUND = '/v1/player/home/background'
// 登录
export const API_LOGIN = '/v1/player/login'
// 登出
export const API_LOGOUT = '/v1/player/logout'
// 获取个人信息
export const API_PROFILE = '/v1/player/profile'
// 获取教练列表
export const API_COACH_LIST = '/v1/coach/list'
// 选择教练
export const API_CHOOSE_COACH = '/v1/player/choose/coach'
// 统计信息
export const API_USER_STATUS = '/v1/player/status'
// 提交脉搏
export const API_PULSE = '/v1/player/pulse'
// 提交训练量
export const API_TRANING_VOLUME = '/v1/player/training/volume'
// 提交训练强度
export const API_TRANING_INTENSITY = '/v1/player/training/intensity'
// 提交心情
export const API_MOOD_STATUS = '/v1/player/mood'
// 提交伤病情况
export const API_INJURY = '/v1/player/injury'
// 获取经期状态
export const API_PERIOD_STATUS = '/v1/player/period/status'
// 提交经期状态
export const API_PERIOD = '/v1/player/period'
// 提交服药情况
export const API_MEDICINE = '/v1/player/medicine'
// 提交日记
export const API_DIARY_SUBMIT = '/v1/player/diary/submit'
// 最新批复日记列表
export const API_DIARY_REPLIED = '/v1/player/diary/replied/latest'
// 获取日记详情
export const API_DIARY_DETAIL = '/v1/player/diary/detail'
// 获取日记历史
export const API_DIARY_HISTORY = '/v1/player/diary/history'
// 更换头像
export const API_AVATAR = '/v1/player/avatar'
// 修改密码
export const API_PASSWORD = '/v1/player/password'
// 反馈信息
export const API_FEEDBACK = '/v1/player/feedback'

export const DIRAY_STEP_API_ALIAS = {
  volume: API_TRANING_VOLUME,
  intensity: API_TRANING_INTENSITY,
  mood: API_MOOD_STATUS,
  injury: API_INJURY,
  period: API_PERIOD,
  medicine: API_MEDICINE
}